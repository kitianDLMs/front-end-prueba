import { Component, OnInit } from '@angular/core';
// Importacion del servicio que nos permitira realizar la peticion al endpoint 
import { ListServiceService } from '../../services/list-service.service';
// Importacion de los modelos 
import { Second } from '../../models/second';
import { SecondData } from '../../models/secondData';

@Component({
  selector: 'app-second-endpoint',
  templateUrl: './second-endpoint.component.html',
  styles: [
  ]
})
export class SecondEndpointComponent implements OnInit {

  public info: SecondData[] = [];  
  public cargando: boolean = false;

  constructor(private listService: ListServiceService) { }

  ngOnInit(): void {
  }

  getSecond(){
    this.cargando = true;
    this.listService.getSecondData()
        .subscribe(
          res => {       
            // En este lugar deberiamos procesar la data recibida como un arreglo para poder
            // definir cuantas veces se repite cada letra en un parrafo
            // pero ignoro la forma de acceder al parrafo desde aqui pero sigo investigando
            this.info = JSON.parse(res.data);   
            this.info.forEach((paragraph)=> {
              console.log(paragraph);            
            });
            // Se imprime la variable info, que hace referencia al endpoint consumido
            console.log(this.info);                           
            this.cargando = false;
          },
          err => console.error(err)
        )
  }
  // con esta funcion es posible conocer el numero de veces que se repite una letra dentro de un string  
  getCaracteres(paragraph: string, caracter: string){
    let pos: number = 0;
    let count: number = 0;
    // se toma la cadena que ingrese por el argumento(en este caso el parrafo) y se busca la letra que deseemos que sera el segundo parametro del indexOf()
    pos = paragraph.indexOf(caracter);
    // luego se hace una iteracion por esta cadena
    // esta condicional quiere decir que mientras la letra se encuantre dentro del string, se siga repitiendo
    while( pos != -1 ){
      // El contador es el que lleva el conteo de las veces repetidas que se ha encontrado tal letra    
      count++;      
      pos = paragraph.indexOf(caracter, pos+1)
    }
    // Finalmente se retorna count con la cantidad de veces que se repite la letra
    return count;
  }

}
