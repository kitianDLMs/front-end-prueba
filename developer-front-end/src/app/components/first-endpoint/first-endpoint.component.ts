import { Component, OnInit } from '@angular/core';
// Importacion del servicio que nos permitira realizar la peticion al endpoint 
import { ListServiceService } from 'src/app/services/list-service.service';
// Importacion del modelo de la data obtenida
import { First } from '../../models/first';

@Component({
  selector: 'app-first-endpoint',
  templateUrl: './first-endpoint.component.html',
  styles: [
  ]
})
export class FirstEndpointComponent implements OnInit {
  // Esta variable 'info' sera la que usaremos para guardar la data del endpoint
  public info:[] = [];
  // Esta variable 'cargando' es la que se encargara de poner el loading en la pagina mientras se obtiene la data
  // Por defecto es false, ya que no necesitamos que el loading este siempre ahi 
  public cargando: boolean = false;

  constructor(private listService: ListServiceService) { }

  ngOnInit(): void {
  }

  getFirst(){
    // Al momento de lanzar el metodo que obtendra la data desde el endpoint la variable cargando se iguala a true y aparecera el loading
    this.cargando = true;
    // Ahora para hacer lo que queremos es necesario usar el metodo de nuestro servicio 'getFirstData()'
    this.listService.getFirstData()
        // Para obtener respuesta nos subscribimos 
        .subscribe(
          // y dependiendo de lo que se obtenga se reciben o una respuesta
          res => {             
            this.info = res.data;            
            this.cargando = false; 
          },                     
          // o un error
          err => console.error(err)
        )
  }

}
 