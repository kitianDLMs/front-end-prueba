import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// En el routing para hacer posible la navegacion es necesario importar los componentes por los cuales queramos navegar
import { FirstEndpointComponent } from './components/first-endpoint/first-endpoint.component';
import { SecondEndpointComponent } from './components/second-endpoint/second-endpoint.component';
// En este caso se importan estos 2 componentes

const routes: Routes = [  
  // Este arreglo es el que contiene nuestras rutas
  {

    path: '',
    redirectTo: '/firstendpoint',
    pathMatch: 'full'
    // Esta ruta es la inicial y siguen las 2 rutas necesarias
  },
  {
    path: 'firstendpoint',
    component: FirstEndpointComponent
  },
  {
    path: 'secondendpoint',
    component: SecondEndpointComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
