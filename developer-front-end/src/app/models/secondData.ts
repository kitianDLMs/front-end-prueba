export interface SecondData {
    paragraph: any;
    number: number;
    hasCopyright: boolean;
}