import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { NavigationComponent } from './components/navigation/navigation.component';
import { FirstEndpointComponent } from './components/first-endpoint/first-endpoint.component';
import { SecondEndpointComponent } from './components/second-endpoint/second-endpoint.component';
// Desde aqui importamos el servicio para hacerlo visible en los componentes
import { ListServiceService } from './services/list-service.service';

@NgModule({
  declarations: [
    AppComponent,
    FirstEndpointComponent,
    SecondEndpointComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    // Esto nos permite tener acceso a los metodos del servicio
    ListServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
