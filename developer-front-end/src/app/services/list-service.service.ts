import { Injectable } from '@angular/core';
// Importacion de la interfaz http client para poder realizar peticiones http a un backend
import { HttpClient } from '@angular/common/http';
// Importacion de los modelos de la data obtenida 
import { First } from '../models/first';
import { Second } from '../models/second';

@Injectable({
  providedIn: 'root'
})
export class ListServiceService {

  // Declaraciones del endpoint dentro de una variable para tener control
  private API_URI_ONE = 'http://patovega.com/prueba_frontend/array.php';
  private API_URI_TWO = 'http://patovega.com/prueba_frontend/dict.php';
  // Se instancia la interfaz de httpclient para acceder a sus metodos
  constructor(private _http: HttpClient) { }  
  // Metodo para obtener los datos desde el primer endpoint; de tipo First
  getFirstData(){
    return this._http.get<First>(`${this.API_URI_ONE}`);
  }
  // Metodo para obtener los datos desde el segundo endpoint; de tipo Second
  getSecondData(){
    return this._http.get<Second>(`${this.API_URI_TWO}`);
  }

}
